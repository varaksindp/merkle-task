const {Router} = require('express')
const {check, validationResult} = require('express-validator')
const config = require('config')
const jwt = require('jsonwebtoken')
const router = Router()
const fs = require('fs');

// /api/auth/login
router.post(
    '/login',
    [
        check('email', 'Incorrect email').isEmail(),
        check('password', 'Incorrect password').isLength({min: 6}),
    ],
    async (req, res) => {
    try {
        // handling validation results
        const errors = validationResult(req)
        if (!errors.isEmpty()) {
            return res.status(400).json({
                errors: errors.array(),
                message: 'Incorrect data, please try again'
            })
        }

        // logic for user logging in
        const { email, password } = req.body
        // search for user in mock DB (users.json)
        let rawdata = fs.readFileSync(config.get('usersDatabase'));
        let candidatesRow = JSON.parse(rawdata);
        const candidatesMap = new Map(Object.entries(candidatesRow.users));

        for (const user of candidatesMap.values()) {
            if (user.password === password && user.email === email) {
                // addigning token to user for 1h usage
                const token = jwt.sign(
                    { userId: user.id },
                    config.get('jwtSecret'),
                    { expiresIn: '1h'}
                )

                res.json({token, id: user.id, message: 'Successfully logged in'})
            } else {
                res.status(400).json({message: 'User not found'})
            }
        }
    } catch (e) {
        res.status(500).json({message: "Something went wrong, please try again."})
    }
})

module.exports = router



