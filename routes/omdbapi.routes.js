const {Router} = require('express')
const config = require('config')
const router = Router()
const axios = require('axios')

// /api/omdbapi/movie
router.post(
    '/movie',
    async (req, res) => {
        try {
            const { search, typeSearch, yearSearch, page } = req.body

            let reqUrl = 'http://www.omdbapi.com';

           // running request to api
            axios.get(reqUrl, {
                params: {
                    s: search,
                    type: typeSearch,
                    y: yearSearch,
                    page: page,
                    apikey: config.get('apiKey')
                }
            })
                .then((data) => {
                    res.json(data.data)
                })
                .catch(() => {
                    res.status(400).json({message: 'Error requesting movies api'})
                });
        } catch (e) {
            res.status(500).json({message: "Something went wrong, please try again."})
        }
    }
)

module.exports = router
