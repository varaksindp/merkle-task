const express = require('express')
const config = require('config')
const fs = require("fs");
const cors = require("cors")

const app = express();

app.use(express.json({ extended: true }));

app.use(cors())
app.options('*', cors())

app.use('/api/auth', require('./routes/auth.routes'))
app.use('/api/omdbapi', require('./routes/omdbapi.routes'))

const PORT = config.get('port') || 5000

async function start() {
    try {
        // add functionality to connect to Redis
    } catch (e) {
        console.log('Server Error', e.message)
        process.exit(1)
    }
}

app.listen(PORT, () => console.log(`app has been started on port ${PORT}...`))


