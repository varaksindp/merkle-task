import React from "react";
import {Route, Routes, Navigate} from "react-router-dom";
import {SearchPage} from "./pages/SearchPage";
import {AuthPage} from "./pages/AuthPage";

export const useRoutes = isAuthenticated => {
    if (isAuthenticated) {
        return (
            <Routes>
                <Route path="/*" element={<Navigate to="/search"/>} />
                <Route path="/search" element={<SearchPage />} />
            </Routes>
        )
    }

    return (
        <Routes>
            <Route path="/*" element={<Navigate to="/auth"/>} />
            <Route path="/auth" element={<AuthPage />} />
        </Routes>
    )
}
