import React, {useContext, useEffect, useState} from "react";
import {useHttp} from "../hooks/http.hook";
import {AuthContext} from "../context/AuthContext";
import {Button, Card, CardActions, CardContent, Typography, TextField} from "@mui/material";
import { ToastContainer, toast } from 'react-toastify';

export const AuthPage = () => {
    const auth = useContext(AuthContext)
    const { loading, error, request, clearError } = useHttp()
    const [form, setForm] = useState({
        email: '', password: ''
    })

    useEffect(() => {
        clearError()
    }, [error, clearError])

    const changeHandler = event => {
        setForm({...form, [event.target.name]: event.target.value })
    }

    const loginHandler = async () => {
        try {
            const data = await request('/api/auth/login', 'POST', {...form})
            toast(data.message)
            auth.login(data.token, data.id)
        } catch (e) {
            toast(e.message)
        }
    }

    return (
        <section id="authPage" className="h-screen flex justify-center items-center bg-gray-200">
            <Card variant="outlined" className="py-6 px-12">
                <CardContent>
                    <Typography gutterBottom variant="h4" component="div" className="text-center">
                        Authorization
                    </Typography>

                    <div className="flex flex-col">
                        <div className="my-2">
                            <TextField
                                id="email"
                                type="text"
                                name="email"
                                label="Email"
                                variant="outlined"
                                onChange={changeHandler}
                            >
                            </TextField>
                        </div>

                        <div className="my-2">
                            <TextField
                                id="password"
                                type="password"
                                name="password"
                                label="Password"
                                variant="outlined"
                                onChange={changeHandler}
                            >
                            </TextField>
                        </div>
                    </div>
                </CardContent>

                <CardActions className="flex justify-center">
                    <Button
                        size="large"
                        variant="contained"
                        onClick={loginHandler}
                        disabled={loading}
                        type="submit"
                        name="action"
                    >
                        Submit
                    </Button>
                </CardActions>
            </Card>
            <ToastContainer />
        </section>
    )
}
