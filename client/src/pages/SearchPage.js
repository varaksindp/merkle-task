import React, {useState, useEffect} from "react";
import {useHttp} from "../hooks/http.hook";
import {debounce} from "lodash";
import { ToastContainer, toast } from 'react-toastify';
import {
    alpha,
    AppBar,
    Button, Card, CardContent, CardMedia,
    CircularProgress,
    Collapse, Pagination,
    styled,
    TextField,
    Toolbar,
    Tooltip,
    Typography
} from "@mui/material";

const Search = styled('div')(({ theme }) => ({
    borderRadius: '3px',
    backgroundColor: alpha(theme.palette.common.white, 0.40),
    '&:hover': {
        backgroundColor: alpha(theme.palette.common.white, 0.50),
    },
    marginLeft: '5px',
    marginRight: '5px',
}));

const OpenBtn = styled('div')(({ theme }) => ({
    borderRadius: '3px',
    backgroundColor: alpha(theme.palette.common.white, 0.90),
}));

let debouncedSearchHandler

export const SearchPage = () => {
    const { loading, error, request, clearError } = useHttp()
    const [isOpen, setIsOpen] = useState(false);
    const [searchReq, setSearchReq] = useState({
        search: '', typeSearch: '', yearSearch: '', page: ''
    })
    const [movies, setMovies] = useState({})
    const [pages, setPages] = useState(0);
    const [activePage, setActivePage] = useState(1);

    useEffect(() => {
        toast(error)
        clearError()
    }, [error, clearError])

    useEffect(() => {
        debouncedSearchHandler = debounce(searchHandler, 300);
    }, [])

    const changeHandler = event => {
        let tempSearchReq = { [event.target.name]: event.target.value }
        setSearchReq({...searchReq, ...tempSearchReq})
        debouncedSearchHandler({...searchReq, ...tempSearchReq})
    }

    const searchHandler = async (searchReq) => {
        try {
            if (searchReq.search) {
                const data = await request('/api/omdbapi/movie', 'POST', searchReq)
                if (data.totalResults > 0) {
                    setMovies(data)
                    setPages(Math.ceil(data.totalResults / 10))
                } else {
                    setMovies({})
                    setPages(0)
                    toast('No movies found on request')
                }
            }
        } catch (e) {
            toast(e.message)
        }
    }

    const handlePageChange = (event) => {
        if (event.target.dataset.testid === "NavigateNextIcon") {
            setActivePage(activePage + 1)
            debouncedSearchHandler({...searchReq, page: activePage})
            return
        }

        if (event.target.dataset.testid === "NavigateBeforeIcon") {
            setActivePage(activePage - 1)
            debouncedSearchHandler({...searchReq, page: activePage})
            return
        }
        setActivePage(+event.target.innerText)
        debouncedSearchHandler({...searchReq, page: activePage})
    }

    return (
        <section id="searchPage">
            <AppBar position="static">
                <Toolbar variant="dense">
                    <div className="absolute left-3 top-5">
                        <Typography variant="h5" color="inherit" component="div">
                            OMDb
                        </Typography>
                    </div>

                    <div className="flex justify-center w-full pt-2 pb-2">
                        <Search>
                            <TextField
                                id="search"
                                type="text"
                                name="search"
                                placeholder="Search a movie"
                                variant="filled"
                                onChange={changeHandler}
                                size="small"
                            >
                            </TextField>
                        </Search>
                    </div>

                    <div className="absolute right-3 top-4">
                        <Tooltip title="Toggle">
                            <OpenBtn>
                                <Button
                                    variant="text"
                                    onClick={() => setIsOpen(!isOpen)}
                                >
                                    {!isOpen && <span>&darr;</span>}
                                    {isOpen && <span>&uarr;</span>}
                                </Button>
                            </OpenBtn>
                        </Tooltip>
                    </div>
                </Toolbar>
                <Collapse in={isOpen} timeout="auto" unmountOnExit>
                    <Toolbar variant="dense">
                        <div className="flex justify-center w-full pb-2">
                            <Search>
                                <TextField
                                    id="typeSearch"
                                    type="text"
                                    name="typeSearch"
                                    placeholder="Type search"
                                    variant="filled"
                                    onChange={changeHandler}
                                    size="small"
                                >
                                </TextField>
                            </Search>

                            <Search>
                                <TextField
                                    id="yearSearch"
                                    type="number"
                                    name="yearSearch"
                                    placeholder="Year search"
                                    variant="filled"
                                    onChange={changeHandler}
                                    size="small"
                                >
                                </TextField>
                            </Search>
                        </div>
                    </Toolbar>
                </Collapse>
            </AppBar>


            {
                loading &&
                <div className="flex justify-center mt-10">
                    <CircularProgress />
                </div>
            }
            {
                Object.keys(movies).length != 0 && !loading &&
                <div className="grid grid-cols-3 gap-6 mt-10 mr-6 ml-6 mb-10">
                    {
                        Object.entries(movies.Search).map(([key,value])=>{
                            return (
                                <Card key={key}>
                                    <Typography gutterBottom variant="h5" component="div" className="text-center">
                                        Title: {value.Title.toString()}
                                    </Typography>
                                    <div className="flex">
                                        <div className="w-1/3">
                                            <CardMedia
                                                component="img"
                                                height="200"
                                                alt="green iguana"
                                                src={value.Poster.toString()}
                                            />
                                        </div>
                                        <div className="w-2/3">
                                            <CardContent>
                                                <Typography variant="body2" color="text.secondary">
                                                    <span className="text-lg">Year: {value.Year.toString()}</span>
                                                    <br/>
                                                    <span className="text-lg">imdbID: {value.imdbID.toString()}</span>
                                                    <br/>
                                                    <span className="text-lg">Type: {value.Type.toString()}</span>
                                                </Typography>
                                            </CardContent>
                                        </div>
                                    </div>
                                </Card>
                            );
                        })
                    }
                </div>
            }
            {
                pages > 1 && !loading &&
                    <div className="flex justify-center mb-6">
                        <Pagination
                            count={pages}
                            shape="rounded"
                            page={activePage}
                            onChange={e => handlePageChange(e)}
                        />
                    </div>
            }
            <ToastContainer />
        </section>
    )
}
